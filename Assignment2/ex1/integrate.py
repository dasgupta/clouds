from math import sin
lower = 0.00
upper = 3.14159
N_list = [10, 100, 100, 1000, 10000, 100000, 1000000]
results = []


def integral(lower : float, upper : float, j : list) -> list:

    dx = (upper - lower) / float(j)
    intg = 0.00
    i = 0

    while i < j:
        intg+=(abs(sin((lower + i*dx)))*dx)
        i=1+i
    return intg


for i in N_list:
    results.append(integral(lower, upper, i))
print(results)





from flask import Flask
from integrate import integral

app = Flask(__name__)

@app.route('/clouds/')
def did_i_get_the_assignement():
    return '<title>works</title><body>works</body>'

@app.route('/numericalintegralservice/<lower>/<upper>')
def service(lower: str, upper: str):
    results = integral(float(lower), float(upper))
    return str(results)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
